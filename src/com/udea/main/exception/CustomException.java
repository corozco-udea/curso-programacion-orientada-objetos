package com.udea.main.exception;

public class CustomException extends Exception {

    private String code;
    private String status;
    private String message;

    public CustomException(String code, String satus, String mesage) {
        super();
        this.code = code;
        this.status = status;
        this.message = mesage;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
