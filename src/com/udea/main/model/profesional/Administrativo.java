package com.udea.main.model.profesional;

import com.udea.main.model.Persona;

public class Administrativo extends Persona {

    private String tarjeta_profesional;

    public String getTarjeta_profesional() {
        return tarjeta_profesional;
    }

    public void setTarjeta_profesional(String tarjeta_profesional) {
        this.tarjeta_profesional = tarjeta_profesional;
    }
}
