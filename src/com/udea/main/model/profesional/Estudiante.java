package com.udea.main.model.profesional;

import com.udea.main.model.Persona;
import com.udea.main.model.PersonaInterface;

public class Estudiante extends Persona implements PersonaInterface {

    private String id_universidad;

    private float promedio;

    public Estudiante(final String id_universidad, final float promedio, final String nombre, final String cedula) {
        super(nombre, cedula);
        this.id_universidad = id_universidad;
        this.promedio = promedio;
    }

    public String getId_universidad() {
        return id_universidad;
    }

    public void setId_universidad(String id_universidad) {
        this.id_universidad = id_universidad;
    }

    public float getPromedio() {
        return promedio;
    }

    public void setPromedio(float promedio) {
        this.promedio = promedio;
    }

    @Override
    public String hablar() {
        return "Hola soy un Estudiante";
    }

    @Override
    public String caminar() {
        return "Hola soy un Estudiante y estoy caminando";
    }
}
