package com.udea.main.model.profesional;

import com.udea.main.model.Persona;

import java.util.List;

public class Profesor extends Persona {

    private String tarjeta_profesional;

    private List<String> cursos;

    private String instituto;

    public String getTarjeta_profesional() {
        return tarjeta_profesional;
    }

    public void setTarjeta_profesional(String tarjeta_profesional) {
        this.tarjeta_profesional = tarjeta_profesional;
    }

    public List<String> getCursos() {
        return cursos;
    }

    public void setCursos(List<String> cursos) {
        this.cursos = cursos;
    }

    public String getInstituto() {
        return instituto;
    }

    public void setInstituto(String instituto) {
        this.instituto = instituto;
    }
}
