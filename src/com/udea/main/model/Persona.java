package com.udea.main.model;

public class Persona {

    //encapsulación (public, protected, private)
    private String cedula; //Atributo
    private String nombre;
    private int edad;

    public Persona() {
        //Default constructor
    }

    public Persona(final String nombre, final String cedula) {
        this.nombre = nombre;
        this.cedula = cedula;
    }

    public Persona(final String nombre) {
        this.nombre = nombre;
    }

    //Getter
    public String getCedula() {
        return this.cedula;
    }

    //Setter
    public void setCedula(final String cedula) {
        this.cedula = cedula;
    }

    //Getter
    public String getNombre() {
        return this.nombre;
    }

    //Setter
    public void setNombre(final String nombre) {
        this.nombre = nombre;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(final int edad) {
        this.edad = edad;
    }

    public void sayHello() {
        System.out.println("Hello, my name is " + (null != this.nombre ? this.nombre : "undefined"));
    }

    @Override
    public String toString() {
        return "Persona{" +
                "cedula='" + this.cedula + '\'' +
                ", nombre='" + this.nombre + '\'' +
                ", edad='" + this.edad + '\'' +
                '}';
    }
}
