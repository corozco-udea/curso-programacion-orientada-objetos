package com.udea.main.model.deportista;

import com.udea.main.model.Persona;

public class NadadorOlimpico extends Persona implements Nadador {

    public NadadorOlimpico(final String nombre) {
        super(nombre);
    }

    public void nadar() {
        System.out.println(this.getNombre() + " está nadando y es olimpico");
    }

}
