package com.udea.main.model.deportista;

import com.udea.main.model.Persona;

public class NadadorIntermedio extends Persona implements Nadador {

    public NadadorIntermedio(final String nombre) {
        super(nombre);
    }

    public void nadar() {
        System.out.println(this.getNombre() + " está nadando y es intermedio");
    }

}
