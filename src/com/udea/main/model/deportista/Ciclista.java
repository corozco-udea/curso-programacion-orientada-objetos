package com.udea.main.model.deportista;

public interface Ciclista {

    void pedalear();
}
