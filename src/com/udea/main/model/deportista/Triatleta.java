package com.udea.main.model.deportista;

import com.udea.main.model.Persona;

public class Triatleta extends Persona implements Ciclista, Nadador, Corredor {

    private String identificador;
    private float peso;
    private float estatura;
    private String categoria;

    public Triatleta() {
        // Default constructor
    }

    public Triatleta(final String nombre, final String cedula, final String identificador, final float peso, final float estatura, final String categoria) {
        super(nombre, cedula);
        this.identificador = identificador;
        this.peso = peso;
        this.estatura = estatura;
        this.categoria = categoria;
    }

    @Override
    public void pedalear() {
        System.out.println(this.getNombre() + " está pedaleando");
    }

    @Override
    public void nadar() {
        System.out.println(this.getNombre() + " está nadando");
    }

    @Override
    public void correr() {
        System.out.println(this.getNombre() + " está corriendo");
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public float getEstatura() {
        return estatura;
    }

    public void setEstatura(float estatura) {
        this.estatura = estatura;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return "Triatleta{" +
                "identificador='" + identificador + '\'' +
                ", peso=" + peso +
                ", estatura=" + estatura +
                ", categoria='" + categoria + '\'' +
                '}';
    }
}
