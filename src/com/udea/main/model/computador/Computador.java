package com.udea.main.model.computador;

import com.udea.main.model.Persona;

import java.util.ArrayList;
import java.util.List;

public class Computador {

    private String id;
    private String referencia;
    private String marca;
    private Persona fabricante;

    //agregation
    private List<Periferico> perifericos;

    //composition
    public Computador(final Persona fabricante) {
        this.fabricante = fabricante;
        this.perifericos = new ArrayList<>();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferencia() {
        return referencia;
    }

    public void setReferencia(String referencia) {
        this.referencia = referencia;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public Persona getFabricante() {
        return fabricante;
    }

    public void setFabricante(Persona fabricante) {
        this.fabricante = fabricante;
    }

    public List<Periferico> getPerifericos() {
        return perifericos;
    }

    public void setPerifericos(List<Periferico> perifericos) {
        this.perifericos = perifericos;
    }

    @Override
    public String toString() {
        return "Computador{" +
                "id='" + id + '\'' +
                ", referencia='" + referencia + '\'' +
                ", marca='" + marca + '\'' +
                ", fabricante=" + fabricante +
                ", perifericos=" + perifericos +
                '}';
    }
}
