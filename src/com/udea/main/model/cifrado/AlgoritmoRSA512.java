package com.udea.main.model.cifrado;

public class AlgoritmoRSA512 implements AlgoritmoCifrado {

    @Override
    public void cifrar(String texto_plano) {
        System.out.println("Cifrando con RSA512");
    }

    @Override
    public String descfrar(String texto_cifrado) {
        return "Texto descifraco con RSA512";
    }
}
