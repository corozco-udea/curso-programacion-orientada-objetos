package com.udea.main.model.cifrado;

public interface AlgoritmoCifrado {

    void cifrar(String texto_plano);

    String descfrar(String texto_cifrado);
}
