package com.udea.main.model.cifrado;

public class AlgoritmoPSA512 implements AlgoritmoCifrado {

    @Override
    public void cifrar(String texto_plano) {
        System.out.println("Cifrando con PSA512");
    }

    @Override
    public String descfrar(String texto_cifrado) {
        return "Texto descifraco con PSA512";
    }
}
