package com.udea.main.model.cifrado;

public class AlgoritmoRSA256 implements AlgoritmoCifrado {


    @Override
    public void cifrar(String texto_plano) {
        System.out.println("Cifrando con RSA256");
    }

    @Override
    public String descfrar(String texto_cifrado) {
        return "Texto descifraco con RSA256";
    }
}
