package com.udea.main.model.generico;

import com.udea.main.exception.CustomException;

public class Lista<T> {

    private Nodo<T> head;
    private Nodo<T> tail;

    private int size;

    public Lista() {
        this.head = null;
        this.tail = null;
        this.size = 0;
    }

    public int size() {
        return this.size;
    }

    public void add(T element) {
        Nodo<T> nodo = new Nodo<>(element);
        if (this.size == 0) {
            this.head = nodo;
            this.tail = this.head;
        } else {
            this.tail.setNext(nodo);
            this.tail = nodo;
        }
        this.size += 1;
    }

    public void add(int index, T element) {
        if(index == this.size -1) {
            add(element);
        } else if(index == 0) {
            final Nodo<T> nodo = new Nodo<>(element);
            final Nodo<T> aux = this.head;
            nodo.setNext(aux);
            this.head = nodo;
        } else {
            //TODO Terminar metodo para elementos en posiciones diferentes a 0 y size
        }
    }

    public T get(int index) throws CustomException {
        if (index >= size) {
            throw new CustomException("500", "ERROR", "IndexOutOfBoundExcepcio: Index " + index + " out of bounds for length " + this.size);
        }

        int i = 0;
        Nodo current = this.head;
        for (i = 0; i < index; i++) {
            current = current.getNext();
        }
        return (T) current.getValor();
    }

    public void print() {
        Nodo current = this.head;
        while(null != current) {
            System.out.print(current.getValor() + (null != current.getNext() ? " -> " : ""));
            current = current.getNext();
        }
        System.out.println();
    }
}
