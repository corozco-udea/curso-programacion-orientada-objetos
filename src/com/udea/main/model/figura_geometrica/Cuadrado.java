package com.udea.main.model.figura_geometrica;

public class Cuadrado extends Figura {

    private double lado;

    public Cuadrado(final double lado, final String color, final String tipo) {
        super(color, tipo);
        this.lado = lado;
    }

    @Override
    public void sayHello() {
        System.out.println("Hola, soy una figura, soy un cuadrado y sobreescribo el método de mi padre");
    }

    @Override
    public void sayHelloChild() {
        System.out.println("Hola, soy una figura y soy un cuadrado");
    }

    @Override
    public double calcularArea() {
        return Math.pow(lado, 2);
    }

    public double getLado() {
        return lado;
    }

    public void setLado(double lado) {
        this.lado = lado;
    }

    @Override
    public String toString() {
        return "Cuadrado{" +
                "lado=" + lado +
                '}';
    }
}
