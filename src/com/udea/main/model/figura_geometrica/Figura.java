package com.udea.main.model.figura_geometrica;

public abstract class Figura {

    private String color;
    private String tipo;

    public Figura(final String color, final String tipo) {
        this.color = color;
        this.tipo = tipo;
    }

    public void sayHello() {
        System.out.println("Hola, soy una figura y no se de que tipo soy");
    }

    public abstract void sayHelloChild();

    public abstract double calcularArea();

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }
}
