package com.udea.main.model.figura_geometrica;

public class Triangulo extends Figura {

    private double base;
    private double h;

    public Triangulo(final double base, final double h, final String color, final String tipo) {
        super(color, tipo);
        this.base = base;
        this.h = h;
    }

    @Override
    public void sayHelloChild() {
        System.out.println("Hola, soy una figura y soy un triangulo");
    }

    public double calcularArea() {
        return (this.base*this.h)/2;
    }

    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getH() {
        return h;
    }

    public void setH(double h) {
        this.h = h;
    }

    @Override
    public String toString() {
        return "Triangulo{" +
                "base=" + base +
                ", h=" + h +
                '}';
    }
}
