package com.udea.main.model;

public interface PersonaInterface {

    String hablar();

    String caminar();
}
