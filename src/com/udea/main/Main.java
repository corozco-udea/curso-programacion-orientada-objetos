package com.udea.main;

import com.udea.main.exception.CustomException;
import com.udea.main.model.Persona;
import com.udea.main.model.cifrado.AlgoritmoCifrado;
import com.udea.main.model.cifrado.AlgoritmoPSA512;
import com.udea.main.model.cifrado.AlgoritmoRSA256;
import com.udea.main.model.cifrado.AlgoritmoRSA512;
import com.udea.main.model.computador.Computador;
import com.udea.main.model.computador.Periferico;
import com.udea.main.model.deportista.*;
import com.udea.main.model.figura_geometrica.Cuadrado;
import com.udea.main.model.figura_geometrica.Figura;
import com.udea.main.model.figura_geometrica.Triangulo;
import com.udea.main.model.generico.Lista;
import com.udea.main.model.generico.Nodo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        final List<Integer> enteros = new ArrayList<>();
        final List<String> strings = new ArrayList<>();

        final Lista<Integer> enterosCustom = new Lista<>();
        final Lista<String> stringCustom = new Lista<>();

        System.out.println("size inicial: " + enterosCustom.size());
        enterosCustom.add(1);
        enterosCustom.add(2);
        enterosCustom.add(3);
        System.out.println("size final: " + enterosCustom.size());


        System.out.println("size inicial: " + enterosCustom.size());
        stringCustom.add("Ana");
        stringCustom.add("Jimena");
        stringCustom.add("Alan");
        stringCustom.add("Carlos");
        System.out.println("size final: " + stringCustom.size());

        enterosCustom.print();
        stringCustom.print();

    }

    private static void apply_polimorfysm_abstract() {
        //Abstraccion -> implementaciones
        final Figura cuadrado = new Cuadrado(5, "Rojo", "Grande");
        final Figura triangulo = new Triangulo(5, 2, "Rojo", "Grande");

        System.out.println("Area del cuadrado: " + cuadrado.calcularArea());
        System.out.println("Area del triangulo: " + triangulo.calcularArea());

        cuadrado.sayHello();
        cuadrado.sayHelloChild();
        triangulo.sayHello();
        triangulo.sayHelloChild();

        final List<Figura> figuras = Arrays.asList(cuadrado, triangulo);
        figuras.forEach(foo -> System.out.println(foo.toString()));
    }

    private static void apply_polimorfysm_interfaces() {

        final Nadador isabel = new NadadorJunior("Isabel");
        final Nadador carlos = new NadadorIntermedio("Carlos");
        final Nadador andres = new NadadorExperto("Andres");
        final Nadador camila = new NadadorOlimpico("Camila");

        final List<Nadador> nadadores = Arrays.asList(andres, camila, isabel, carlos);
        nadadores.forEach(foo -> foo.nadar());
    }

    private static void get_info_deportista() {
        final Triatleta ana = new Triatleta("Ana", "10611111111", "001", (float) 75.0, (float) 170.0, "Basico");
        ana.correr();
        System.out.println(ana.toString());

        final Triatleta jimena = new Triatleta("Jimena", "1062222222", "002", (float) 60.0, (float) 165.0, "Intermedio");
        System.out.println(jimena.toString());

        final Triatleta alan = new Triatleta();
        alan.setNombre("Alan");
        alan.setCedula("10613333333");
        alan.setIdentificador("003");
        alan.setPeso((float) 75.0);
        alan.setEstatura((float) 178.0);
        alan.setCategoria("Avanzado");
        System.out.println(alan.toString());
    }

    private static void get_inhetirance() {

        final AlgoritmoCifrado rsa512 = new AlgoritmoRSA512();
        rsa512.cifrar("");
        System.out.println(rsa512.descfrar(""));

        final AlgoritmoCifrado rsa256 = new AlgoritmoRSA256();
        rsa256.cifrar("");
        System.out.println(rsa256.descfrar(""));

        final AlgoritmoCifrado psa512 = new AlgoritmoPSA512();
        psa512.cifrar("");
        System.out.println(psa512.descfrar(""));
    }

    private static void get_aggregation_and_composition() {
        System.out.println("Hola mundo orientado a objetos");

        final Persona jimena = new Persona("Jimena", "10617721111");
        final Persona alan = new Persona("Alan", "10617722222");
        final Persona andres = new Persona("Andres", "1061773333");
        final Persona carlos = new Persona("Carlos", "1061774444");

        final List<Persona> personas = new ArrayList<>();
        personas.addAll(Arrays.asList(jimena, alan, andres, carlos));

        int edad = 0;
        for (Persona p : personas) {
            p.setEdad(edad);
            edad += 10;
            System.out.println(p.toString());
        }

        jimena.sayHello();

        Persona empty = new Persona();
        empty.sayHello();

        final Computador comp = new Computador(new Persona());
        comp.getPerifericos().add(new Periferico());
        comp.getPerifericos().add(new Periferico());
        System.out.println(comp.toString());


    }

    private static void method_with_errors() throws IOException {

        double a = 10.5;
        double b = 0;
        try {
            final double c = a / b;
            System.out.println(String.format("%f, %f, %f", a, b, c));
            System.out.println(String.format("%d, %d, %f", (int) a, (int) b, c));

            int c_int = (int) a / (int) b;

            final BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Ingrese un valor numérico: ");
            final int primer_termino = Integer.parseInt(br.readLine());
            System.out.print("Ingrese otro valor numérico: ");
            final int segundo_termino = Integer.parseInt(br.readLine());
            final int result = primer_termino + segundo_termino;
            System.out.println(result);

        } catch (RuntimeException e) {
            // e.printStackTrace();
            System.out.println("Hubo un error en tiempo de ejecución");
        } finally {
            System.out.println("finalmente...");
            b = 15;
            double c_double = a / b;
            System.out.println(String.format("%f, %f, %f", a, b, c_double));
        }

        System.out.println("Sigue el flujo de ejecución");
    }
}

